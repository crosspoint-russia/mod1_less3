<?php

require __DIR__ . '/lib/functions.php'; // подключаем библиотеку функций

/* 
Напишите программу-калькулятор
1. Форма для ввода двух чисел, выбора знака операции и кнопка "равно"
2. Данные пусть передаются методом GET из формы (да, можно и так!) на скрипт, который их примет и выведет выражение и его результат <- перезаписано п.3
3. Попробуйте улучшить программу. Пусть данные отправляются на ту же страницу на PHP, введенные числа останутся в input-ах, а результат появится после кнопки "равно"
*/

?>

<form id="data" method="get" action="/mod1_less3.php">
	
	<input type="text" name="a" value="<?php if (isset($_GET['a'])) { echo $_GET['a']; } ?>" /> <!-- подставляем значение, предварительно проверив его существование -->
	
	<select name="operation">
	<option value="+" <?php if (isset($_GET['operation']) && '+' == $_GET['operation']) { ?> selected="selected" <?php } ?>>+</option> <!-- здесь и далее проверка и выбор знака -->
	<option value="-" <?php if (isset($_GET['operation']) && '-' == $_GET['operation']) { ?> selected="selected" <?php } ?>>-</option>
	<option value="*" <?php if (isset($_GET['operation']) && '*' == $_GET['operation']) { ?> selected="selected" <?php } ?>>*</option>
	<option value="/" <?php if (isset($_GET['operation']) && '/' == $_GET['operation']) { ?> selected="selected" <?php } ?>>/</option>
	</select>

	<input type="text" name="b" value="<?php if (isset($_GET['b'])) { echo $_GET['b']; }; ?>" /> <!-- подставляем значение, предварительно проверив его существование -->

	<button send="send">=</button>

<?php 
	if (isset($_GET['a'], $_GET['b'], $_GET['operation'])) { // запускаем вывод результата работы функции только если мы получили все нужные GET параметры и они хоть что то содержат 

		$result = formCalc($_GET['a'], $_GET['operation'],  $_GET['b']);

		if (!is_array($result)) {	// ловим массив с ошибками 
		 	echo $result;
		 } else {
		 	echo showError($result['errCode']); // показываем текст ошибки
		 }

	}
?>
</form>


<!-- 
Создайте примитивную фотогалерею из двух страниц
1. Пусть на главной странице галереи выводятся 3-4 изображения
2. Каждое из них пусть будет ссылкой вида /image.php?id=42, где 42 - условный номер изображения
3. На странице image.php вы по номеру понимаете, какое изображение вывести в браузер и выводите его. Я ожидаю, что для этого пункта программы вы создатите массив вида [1 => 'cat.jpg', 2=>'dog.jpg', ... ], 
однако вы можете предложить и другое решение
 -->

<!-- подключаем css стили -->
<link rel="stylesheet" href="/lib/style.css" />

<?php
$images = include(__DIR__ . '/lib/imgDb.php');  // получаем массив с картинками
foreach ($images['imageData'] as $key => $image) { // проходим по вложенному массиву с картинками циклом 
?>

	<div class="imageItem">
		<a href="/image.php?id=<?php echo $key; ?>"><img src="/<?php echo $images['imageDir'] . '/' . $image; ?>"></a>
	</div>

<?php
}
?>