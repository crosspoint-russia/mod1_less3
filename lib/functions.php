<?php

function formCalc($a, $operation, $b) {
	switch ($operation) { // перебор по операциям
		case '+':
			return $a + $b;
		break;
		case '-':
			return $a - $b;	
		break;
		case '*':
			return $a * $b;
		break;
		case '/':
			if(0 == $b) { // возврат при попытке деления на ноль
				return ['errCode' => 0];
			}
			return $a / $b;
		break;
		default:
			return ['errCode' => 1];
		break;
	}
}

/* тесты */

assert(0 == formCalc(0, '+', '0'));
assert(0 == formCalc(500, '*', '0vaeea'));
assert(5 == formCalc(25, '/', 5));
assert(25 == formCalc(30, '-', 5));

// функция отдает текст ошибки по её коду
function showError($errCode) {

$errors = [0 => 'попытка деления на ноль', 1 => 'непредвиденная ошибка'];
return $errors[$errCode];

}

?>